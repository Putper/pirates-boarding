﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatController : MonoBehaviour
{
	public float speed = 1;
	new private Rigidbody rigidbody;


	void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
	}


	void FixedUpdate()
	{
		float horizontal_input = Input.GetAxis("Horizontal");
 		float vertical_input = Input.GetAxis("Vertical");
 		rigidbody.velocity = new Vector3 (horizontal_input * speed, rigidbody.velocity.y, vertical_input * speed);
	}
}
