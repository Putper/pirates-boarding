﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public float zoom;
	public GameObject target;
	public Vector3 offset;

	private void Update()
	{
		transform.position = new Vector3(target.transform.position.x - offset.x, offset.y, target.transform.position.z - offset.z);
	}
}
