﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterController : MonoBehaviour
{
	public float power = 1.5f;
	public float scale = 1;
	public float time_scale = 0.5f;

	private float offset_x;
	private float offset_y;
	private MeshFilter mesh_filter;
	private MeshCollider mesh_collider;

	private void Awake()
	{
		mesh_filter = GetComponent<MeshFilter>();
		mesh_collider = GetComponent<MeshCollider>();
		SetNoise();
	}

	private void Update()
	{
		SetNoise();
		offset_x += Time.deltaTime * time_scale;
		offset_y += Time.deltaTime * time_scale;
	}

	
	private void SetNoise()
	{
		Vector3[] vertices = mesh_filter.mesh.vertices;

		for(int i=0; i < vertices.Length; i++)
		{
			vertices[i].y = CalculateHeight(vertices[i].x, vertices[i].z) * power;
		}
		mesh_filter.mesh.vertices = vertices;
		mesh_collider.sharedMesh = mesh_filter.mesh;
	}



	private float CalculateHeight(float x, float y)
	{
		float x_coord = x * scale + offset_x;
		float y_coord = y * scale + offset_y;

		return Mathf.PerlinNoise(x_coord, y_coord);
	}
}
