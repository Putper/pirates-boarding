﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPlaneGenerator : MonoBehaviour
{
	public float size = 128;
	public int grid_size = 128;
	private MeshFilter mesh_filter;
	private MeshCollider mesh_collider;


	private void Awake()
	{
		mesh_filter = GetComponent<MeshFilter>();
		mesh_collider = GetComponent<MeshCollider>();
		mesh_filter.mesh = GenerateMesh();
		mesh_collider.sharedMesh = mesh_filter.mesh;
	}

	private void Update()
	{

	}

	private Mesh GenerateMesh()
	{
		Mesh mesh = new Mesh();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();

		for(int x=0; x < grid_size+1; x++)
		{
			for(int y=0; y < grid_size+1; y++)
			{
				vertices.Add( new Vector3(
					-size * 0.5f + size * (x/(float)grid_size),
					0,
					-size * 0.5f + size * (y/(float)grid_size)
				));
				normals.Add(Vector3.up);
				uvs.Add(new Vector2(x/(float)grid_size, y/(float)grid_size));
			}
		}

		List<int> triangles = new List<int>();
		int vertices_count = grid_size + 1;

		for(int i=0; i < vertices_count*vertices_count-vertices_count; i++)
		{
			if((i+1)%vertices_count == 0)
				continue;

			triangles.AddRange(new List<int>() {
				i+1+vertices_count, i+vertices_count, i,
				i, i+1, i+vertices_count+1
			});
		}

		mesh.SetVertices(vertices);
		mesh.SetNormals(normals);
		mesh.SetUVs(0, uvs);
		mesh.SetTriangles(triangles, 0);

		return mesh;
	}
}
