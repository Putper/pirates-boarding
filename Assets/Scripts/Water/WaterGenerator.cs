﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator : MonoBehaviour
{
	public int grid_size;
	public float plane_size;
	public GameObject water;
	public GameObject player;
	private WaterPlaneGenerator water_generator;
	private WaterController water_controller;
	private Vector3 last_generation_position;

	private void Awake()
	{
		water_generator = water.GetComponent<WaterPlaneGenerator>();
		water_controller = water.GetComponent<WaterController>();
		water_generator.size = plane_size;
		water_generator.grid_size = grid_size;
		last_generation_position = player.transform.position;
		last_generation_position.y = 0;
		Instantiate(water, new Vector3(player.transform.position.x, 0, player.transform.position.z), Quaternion.identity);
	}

	private void FixedUpdate()
	{
		CreateWater();
	}


	private void CreateWater()
	{
        Vector3 down = transform.TransformDirection(Vector3.down);
		RaycastHit hit;

		if(Physics.Raycast(player.transform.position, down, out hit))
		{
			
		}
		 
	}

	private void CreateWaterOld(bool initial = false)
	{
		float distance = Vector3.Distance(last_generation_position, player.transform.position);
		Vector3 difference = last_generation_position - player.transform.position;

		Vector3 offset_new_position = new Vector3();
		bool create_new_water = false;

		if( difference.x/2 > plane_size/2 )
		{
			offset_new_position.x += plane_size;
			create_new_water = true;
		}
		if( difference.z/2 > plane_size/2 )
		{
			offset_new_position.z += plane_size;
			create_new_water = true;
		}

		if(create_new_water || initial)
		{
			last_generation_position = last_generation_position + offset_new_position;
		}
	}
}
